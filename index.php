<?php
# Informações sobre o banco de dados:
$db_host = "localhost";
$db_nome = "provadavi";
$db_usuario = "root";
$db_senha = "";
$db_driver = "mysql";
$erro = true;

$erro_validacao = [];

try {
    # Atribui o objeto PDO à variável $db.
    $db = new PDO("$db_driver:host=$db_host; dbname=$db_nome", $db_usuario, $db_senha);
    # Garante que o PDO lance exceções durante erros.
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    # Garante que os dados sejam armazenados com codificação UFT-8.
    $db->exec('SET NAMES utf8');
} catch (PDOException $e) {
    $erro = "Erro não tratado: " . $e->getMessage();
}

if ($erro == true) {
    if (isset($_POST['cadastrar'])) {

        //inicio das validações
        $dados = filter_input_array(INPUT_POST, FILTER_DEFAULT);
        // var_dump($dados);
        //valida o nome
        if (strlen($dados['nm']) < 10) {
            $erro_validacao[] = "Informe um nome valido";
        }
        //Valida Email
        if(!filter_var($dados['email'], FILTER_VALIDATE_EMAIL)){
           $erro_validacao[] = "Informe um email valido"; 
        }
        if(strlen($dados['senha']) <= 5 || strlen($dados['senha']) >= 15){
           $erro_validacao[] = "Informe uma senha valida!!!"; 
        }
        //Valida a senha
        if($dados['senha'] != $dados['senha-confirmacao']){
           $erro_validacao[] = "As senhas não conferem!!!"; 
        }
        
        if(count($erro_validacao)  == 0){
             $autorizado = 0;
            if(isset($dados['autorizacao'])){
                $autorizado = 1;
            }
            
            $sql = "INSERT INTO usuario (nm, email, senha, autorizado) VALUES (:nm , :email , :senha , :autorizado)";
            
            $db->beginTransaction();
            $preparacao = $db->prepare($sql);
            $preparacao->bindValue(":nm", $dados['nm']);
            $preparacao->bindValue(":email", $dados['email']);
            $preparacao->bindValue(":senha", $dados['senha']);
            $preparacao->bindValue(":autorizado", $autorizado);
            if($preparacao->execute()){
                 $db->commit();
                  $erro_validacao[] = "AHOOOOOBA Tirei 10!!!";
            }else{
                 $db->rollBack();
            }
           
            
        }
        
    }
}
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <!-- Meta tags Obrigatórias -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="css/style.css">
        <link rel="icon" href="img/bookmark.png">
        <title>Prova Davi</title>
    </head>
    <body>
        <form method="POST">

            <div id="conteudo">
                <table border="0" width="100%" style="padding-left:  20px">
                    <tr >
                        <th width="50%" height="80px"class="alinhamento titulo"  > Cadastre-se</th>
                        <th  height="80px"></th>
                    </tr>
                    <tr>
                        <td width="50%" height="80px" > 
                            Nome:<br>
                            <input type="text" name="nm" required>
                        </td>
                        <td>
                            Email:<br>
                            <input type="email" name="email" required>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            Senha:<br>
                            <input type="password" name="senha"  required></td>
                        <td>
                            Repita a senha:<br>
                            <input type="password" name="senha-confirmacao" required></td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" >
                            <input style="margin-top: 20px" type="checkbox" name="autorizacao" value="ON" checked="checked" /> Autorizo o recebimento 
                            de informativos sobre o PJB e seus´parceiros;
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
<?php
foreach ($erro_validacao as $key => $value) {
    echo $value . "<br>";
}
?>
                        </td>
                        <td>  <button type="reset" name="resetar" value="resetar" style=" height: 30px"> FECHAR</button>
                            <button type="submit" name="cadastrar" value="cadastrar" style="background-color: blue; height: 30px"> CADASTRAR</button>

                        </td>
                    </tr>

                </table>

            </div>
        </form>
    </body>

</html>